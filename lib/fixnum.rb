require 'prime'

class Fixnum
  def prime_no_list
    self >= 2 ? [2] + 3.step(self, 2).select { |number| number.prime? } : []
  end
end

